import pygame
pygame.init()
win = pygame.display.set_mode ((1600, 833))
pygame.display.set_caption('Моя первая игруля')

walkRight = [pygame.image.load ('1.png'), pygame.image.load ('2.png'), pygame.image.load ('3.png'), pygame.image.load ('4.png'), pygame.image.load ('5.png'), pygame.image.load ('6.png'), pygame.image.load ('7.png'), pygame.image.load ('8.png'), pygame.image.load ('9.png'), pygame.image.load ('10.png'), pygame.image.load ('11.png'), pygame.image.load ('12.png'), pygame.image.load ('13.png'), pygame.image.load ('14.png')]

walkLeft = [pygame.image.load ('1back'), pygame.image.load ('2back'), pygame.image.load ('3back'), pygame.image.load ('4back'), pygame.image.load ('5back'), pygame.image.load ('6back'), pygame.image.load ('7back'), pygame.image.load ('8back'), pygame.image.load ('9back'), pygame.image.load ('10back'), pygame.image.load ('11back'), pygame.image.load ('12back'), pygame.image.load ('13back'), pygame.image.load ('14back')]
bg = pygame.image.load ('idle')
playerStand = pygame.image.load ('9.png')

clock = pygame.time.Clock()

x = 50
y = 750
width = 198
height = 198
speed = 15

isJump = False
jumpcount = 10

left = False
right = False
animCount = 0

def drawWindow ():
    global animCount
    win.blit (bg, (0, 0))

    if animCount +1 >= 28:
        animCount = 0

    if left:
        win.blit(walkLeft[animCount // 2], (x, y))
        animCount += 1
    elif right:
        win.blit(walkRight[animCount // 2], (x, y))
        animCount += 1
    else:
        win.blit(playerStand, (x, y))

    
    pygame.display.update()
    

run = True
while  run:
    clock.tick(28)

    for event in pygame.event.get ():
        if event.type == pygame.QUIT:
            run = False
    keys = pygame.key.get_pressed()
    if keys [pygame.K_LEFT] and x > 5:
        x -= speed
        left = True
        right = False
    elif keys [pygame.K_RIGHT] and x < 1600 - width - 10:
        x += speed
        left = False
        right = True
    else:
        left = False
        right = False
        animCount = 0

    if not (isJump):
        if keys [pygame.K_SPACE]:
            isJump = True
       
    else:
        if jumpcount >= -10:
            if jumpcount < 0:
                y += jumpcount ** 2 / 3
            else:
                y -= jumpcount ** 2 / 3
            jumpcount -= 1
        else:
            isJump = False
            jumpcount = 10

drawWindow ()

pygame.quit()
