import pygame
pygame.init()
win = pygame.display.set_mode ((1600, 900))#, pygame.FULLSCREEN)
pygame.mixer.music.load ('Vexento - Amor.mp3')
pygame.mixer.music.play()
#sound1 = pygame.mixer.Sound('s1.mp3')


pygame.display.set_caption('Моя первая игруля')

walkRightRed = [
    pygame.image.load ('1.png'),
    pygame.image.load ('2.png'),
    pygame.image.load ('3.png'),
    pygame.image.load ('4.png'), pygame.image.load ('5.png'), pygame.image.load ('6.png'), pygame.image.load ('7.png'), pygame.image.load ('8.png'), pygame.image.load ('9.png'), pygame.image.load ('10.png'), pygame.image.load ('11.png'), pygame.image.load ('12.png'), pygame.image.load ('13.png'), pygame.image.load ('14.png')]

walkLeftRed = [pygame.image.load ('1back.png'), pygame.image.load ('2back.png'), pygame.image.load ('3back.png'), pygame.image.load ('4back.png'), pygame.image.load ('5back.png'), pygame.image.load ('6back.png'), pygame.image.load ('7back.png'), pygame.image.load ('8back.png'), pygame.image.load ('9back.png'), pygame.image.load ('10back.png'), pygame.image.load ('11back.png'), pygame.image.load ('12back.png'), pygame.image.load ('13back.png'), pygame.image.load ('14back.png')]

walkRightGreen = [pygame.image.load ('1g.png'), pygame.image.load ('2g.png'), pygame.image.load ('3g.png'), pygame.image.load ('4g.png'), pygame.image.load ('5g.png'), pygame.image.load ('6g.png'), pygame.image.load ('7g.png'), pygame.image.load ('8g.png'), pygame.image.load ('9g.png'), pygame.image.load ('10g.png'), pygame.image.load ('11g.png'), pygame.image.load ('12g.png'), pygame.image.load ('13g.png'), pygame.image.load ('14g.png')]

walkLeftGreen = [pygame.image.load ('1gback.png'), pygame.image.load ('2gback.png'), pygame.image.load ('3gback.png'), pygame.image.load ('4gback.png'), pygame.image.load ('5gback.png'), pygame.image.load ('6gback.png'), pygame.image.load ('7gback.png'), pygame.image.load ('8gback.png'), pygame.image.load ('9gback.png'), pygame.image.load ('10gback.png'), pygame.image.load ('11gback.png'), pygame.image.load ('12gback.png'), pygame.image.load ('13gback.png'), pygame.image.load ('14gback.png')]
start = pygame.image.load ('start.png')
worm = pygame.image.load ('worm.png')
bg = pygame.image.load ('idle.png')
playerStandRed = pygame.image.load ('9.png')
playerStandGreen = pygame.image.load ('9gback.png')
clock = pygame.time.Clock()

xRed = 50
yRed = 500
xGreen = 1400
yGreen = 500
xWorm = 750
yWorm = 20



widthRed = 198
heightRed = 198
speedRed = 20

widthGreen = 198
heightGreen = 198
speedGreen = 20

widthWorm = 100
heightWorm = 91
speedWorm = 20

isJumpRed = False
jumpcountRed = 10
isJumpGreen = False
jumpcountGreen = 10

leftRed = True
rightRed = True
leftGreen = True
rightGreen = True
downWorm = True

animCountRed = 0
lastMoveRed = "right"
animCountGreen = 0
lastMoveGreen = "right"
animCountWorm = 0

flag = True 
screen_id = 0

    
def drawStartScreen():  
    global flag
    win.blit (start, (0, 0))
    pygame.display.update()
    keys = pygame.key.get_pressed()
    if keys [pygame.K_RETURN]:
        flag = False
    
def drawGame ():
    global animCountRed
    win.blit (bg, (0, 0))

    if leftRed:
        win.blit(walkLeftRed[animCountRed // 2], (xRed , yRed ))
        animCountRed += 1
        if animCountRed +1 >= 28:
            animCountRed = 0
    elif rightRed:
        win.blit(walkRightRed[animCountRed // 2], (xRed , yRed ))
        animCountRed += 1
        if animCountRed +1 >= 28:
            animCountRed = 0
    else:
        win.blit(playerStandRed, (xRed, yRed))

    

    global animCountGreen
    if leftGreen:
        win.blit(walkLeftGreen[animCountGreen // 2], (xGreen , yGreen ))
        animCountGreen += 1
        if animCountGreen +1 >= 28:
            animCountGreen = 0
    elif rightGreen:
        win.blit(walkRightGreen[animCountGreen // 2], (xGreen , yGreen ))
        animCountGreen += 1
        if animCountGreen +1 >= 28:
            animCountGreen = 0
    else:
        win.blit(playerStandGreen, (xGreen, yGreen))

    global animCountWorm
    if downWorm:
        win.blit (worm,(xWorm, yWorm))
        animCountWorm += 1
        if animCountWorm +1 >= 28:
            animCountWorm = 0


    pygame.display.update()





       


run = True

while  run:
    clock.tick(28)

    for event in pygame.event.get ():
        if event.type == pygame.QUIT:
            run = False

    
    keys = pygame.key.get_pressed()

    if flag:
        drawStartScreen()
    else:
        drawGame()

        if keys [pygame.K_a] and xRed > 50:
            xRed -= speedRed
            leftRed = True
            rightRed = False
            lastMoveRed = "left"
        elif keys [pygame.K_d] and xRed < 1600 - widthRed - 10:
            xRed += speedRed
            leftRed = False
            rightRed = True
            lastMoveRed = "right"
            
        else:
            leftRed = False
            rightRed = False
            animCountRed = 0

        if not (isJumpRed):
            if keys [pygame.K_w]:
                isJumpRed = True
        
        else:
            if jumpcountRed >= -10:
                if jumpcountRed < 0:
                    yRed += jumpcountRed ** 2 / 2
                else:
                    yRed -= jumpcountRed ** 2 / 2
                jumpcountRed -= 1
            else:
                isJumpRed = False
                jumpcountRed = 10
        



        
        if keys [pygame.K_LEFT] and xGreen > 50:
            xGreen -= speedGreen
            leftGreen = True
            rightGreen = False
            lastMoveGreen = "left"
        elif keys [pygame.K_RIGHT] and xGreen < 1600 - widthGreen - 10:
            xGreen += speedGreen
            leftGreen = False
            rightGreen = True
            lastMoveGreen = "right"
            
        else:
            leftGreen = False
            rightGreen = False
            animCountGreen = 0

        if not (isJumpGreen):
            if keys [pygame.K_UP]:
                isJumpGreen = True
        
        else:
            if jumpcountGreen >= -10:
                if jumpcountGreen < 0:
                    yGreen += jumpcountGreen ** 2 / 2
                else:
                    yGreen -= jumpcountGreen ** 2 / 2
                jumpcountGreen -= 1
            else:
                isJumpGreen = False
                jumpcountGreen = 10


        if animCountWorm > 0:
            yWorm += speedWorm
            downWorm = True


pygame.quit()
