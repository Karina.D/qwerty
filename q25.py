import random
import utils

n = int(input())
print ('-----')
a = utils.createRandomList(n)
utils.printList(a)  

def minIndex(a, j):
    min = j
    for i in range (j, len(a), 1):
        if a[min] < a[i]:
            min = i
    return min

def Sort (a):
    for i in range (0, len(a), 1):
        j = minIndex (a, i)
        t = a[i]
        a[i] = a[j]
        a[j] = t
    return a
q = Sort (a)
print (q)

 