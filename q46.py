import json


s = {
    'Имя': 'Вася',
    'Ф': 'Вася',
    'О': 'Вася',
    'DATA': [1, 2, 3]
}

with open('fileD.json', 'w') as f:
    json.dump(s, f, ensure_ascii = False, indent = 4)
    #s = данные,  f =  файл,  ensure_ascii = False - переводит на русский язык, ident = 4 - табулирует

