import random

def createRandomList (n):
    x = []
    for i in range(0, n, 1):
        a = random.randint(0, 10)
        x. append (a)   
    return x

def printList(x):

    for i in range (0, len(x), 1):
        print(x[i])

def getSumm(x):
    sum = 0
    for q in range (0, len (x), 1):
        sum = sum + x[q]
    return sum


def MinMax(x):
    min = x[0]
    for i in range (1, len(x), 1):
        if x[i] > min:
            min = x[i]
    return min

def minIndex(a, j):
    min = j
    for i in range (j, len(a), 1):
        if a[min] > a[i]:
            min = i
    return min

def createRandomList2D (n, m):
    a2 = []
    for i in range (0, n, 1):
        a1 = []
        for j in range (0, m, 1):
            a1. append (random.randint(0, 5))
        a2. append(a1)
    return a2

def printList2D (list2d):
    for i in range (0, len (list2d), 1):
        for j in range (0, len(list2d[i]), 1):
            print (list2d[i][j], end = '\t')
        print()

def getSumm2D(list2d):
    sum = 0
    for i in range (0, len (list2d), 1):
        for j in range (0, len(list2d [i]), 1):
            sum = sum + list2d[i][j]
    return sum

def Summ3d (a2, b2):
    res = []
    for i in range (0, len (a2), 1):
        res1 = []
        for j in range (0, len (b2), 1):
            res1. append (a2[i][j] + b2[i][j])
        res. append (res1)
    return res

def Multilist (b2, a2):
    res = []
    q = 0
    for i in range (0, len (b2), 1):
        res1 = []
        for j in range (0, len (a2[0]), 1):
            q1 = 0
            for k in range(0, len(b2[0]), 1):
                q1 += a2[k][j] * b2[i][k]
            res1. append (q1)
        res. append (res1)   
    return (res)

